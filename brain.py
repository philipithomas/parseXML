import xml.etree.ElementTree as e
from urllib import urlopen

# This file iterates through the pages of the below atom feed 
# and exports the data as SQL and CSV files.


# Intialize SQL and CSV write sequences - 
# we do this so that we can iteratively add
# on data, then we write it at the end of the
# script into .sql and .csv files, repsectively

#Columns
header_line= "listing_id,title,region_full,weekly_rate_from,weekly_rate_to,monthly_rate_from,monthly_rate_to,nightly_weekday_rate_from,nightly_weekday_rate_to,nightly_weekend_rate_from,nightly_weekend_rate_to"
#SQL Table Name - modify this to update the insert statement
table = "table"

sql="insert into " +  table + " (" + header_line + ") values \n"

# put in header row in csv
csv=header_line + "\n"

#Iterate through pages 1-10
for i in range(1,11):
	# Here is the url
	# Alternatively, we can use a "while" statement increasing the page
	# number on each iteration until the page 404s
	url = "https://partner.homeaway.com/aggregator/homeaway/partners/search?partnerId=cj_all&format=cjAtom&affiliateId=123456&page=%r" % i

	# Just based on the atom feed, each tag is prepended with this. 
	# I don't know why. But this fixes it.
	prefix="{http://www.w3.org/2005/Atom}"

	# First parse the url
	tree = e.parse(urlopen(url))

	# Now we go to the root of the file
	root = tree.getroot()


	# The page has 10 entries / page. We 
	# iterate through them. 
	# Note that we do not do this numerically, because 
	# the last page could have <10 entries.
	for entry in root.findall( prefix + "entry"):
		
		## First, get the parts of the listing_id 
		# XPath: feed/entry/content/listing
		# @systemId + @propertyId + @unitId
		
		systemId = entry.find( prefix + "content").find( prefix + "listing").get('systemId')
		propertyId=entry.find(prefix+"content").find(prefix+"listing").get('propertyId')
		unitId=entry.find(prefix+"content").find(prefix+"listing").get('unitId')
		
		# then piece together the listing_id
		
		listing_id=systemId + "-" + propertyId + "-" + unitId
	

		## Next column: title
		# xpath: feed/entry/title
		title= entry.find(prefix + "title").text
	
	
		## Next we build the region_full:
		# xpath: feed/entry/content/listing/regions/region
		
		# Select all the regions:

		# set a counter and initiate a blank region_full
		region_count = 1
		region_full=""
	
		# Loop through and only add a slash separation if it is not the first entry
		# ( to match the desired output format)
		for region in entry.find(prefix+"content").find(prefix+"listing").find(prefix + "regions").findall(prefix+"region"):
			if (region_count!=1):
				# Only if not first line: Add parenthesis delimiter
				region_full+=" / "
				
			# Out of loop - add name of region
			region_full+=region.text

			# increase count
			region_count+=1
		
		# We should now have the complete region_full
	
		# Note from here on: 
		# Not all of the listings have a rate for each
		# time period, so we initiate as null, then overwrite
		# if the value exists. If we query for each period 
		# as we create the variable, we run into a problem:
		# The xml parsing library returns a value of "None", 
		# rather than null, if the value doesn't exist, 
		# so we would have to check every call and compare
		# it against the value "None", then overwrite to null.
	
		# Initialize null variables:
	
		#We'll overwrite them if they exist.
		weekly_rate_from=""
		weekly_rate_to=""
		monthly_rate_from=""
		monthly_rate_to=""
		nightly_weekday_rate_from=""
		nightly_weekday_rate_to=""
		nightly_weekend_rate_from=""
		nightly_weekend_rate_to=""
	
		## Get Rates and overwrite the null values
		for rate in entry.find(prefix+'content').find(prefix+'listing').find(prefix+'rates').findall(prefix+'rate'):
			# xpath: feed/entry/content/listing/rates/rate
			
			period=rate.get("periodType")
			# Every rate has a from and to rate - we pull them here just to minimize text below
			rateFrom=rate.get('from')
			rateTo=rate.get('to')
		
			if period=="weekly":
				weekly_rate_from=rateFrom
				weekly_rate_to=rateTo
			
			elif period=="monthly":
				monthly_rate_from=rateFrom
				monthly_rate_to=rateTo
		
			elif period=="nightly-weekday":
				nightly_weekday_rate_from=rateFrom
				nightly_weekday_rate_to=rateTo
			
			elif period=="nightly-weekend":
				nightly_weekend_rate_from=rateFrom
				nightly_weekend_rate_to=rateTo
			
		# We should now have all of the rates completed.
		
		# All of the data is now accumulated for this row - we now 
		# insert it into the CSV and SQL data we are about to write
	
		line =  '"' + listing_id + '"' + "," + '"' + title + '"' + "," + '"' + region_full + '"' + "," + '"' + weekly_rate_from + '"' + "," + '"' + weekly_rate_to + '"' + "," + '"' + monthly_rate_from + '"' + "," + '"' + monthly_rate_to + '"' + "," + '"' + nightly_weekday_rate_from + '"' + "," + '"' + nightly_weekday_rate_to + '"' + "," + '"' + nightly_weekend_rate_from + '"' + "," + '"' + nightly_weekend_rate_to + '"'
		
		#Insert it into the CSV:
		csv+=line +"\n"
	
		#Add an insert statement to the SQL
		sql+= "(" + line + "), \n"
	
	
# Note: Running this overwrites previous output.csv and output.sql files! 

# Now write the CSV to the file output.csv
file = open("output.csv", "w")
file.write(csv)
file.close()

# And write the SQL to the file output.sql
file = open("output.sql", "w")
file.write(sql)
file.close()

# In the future, the writing should be done on each iteration to minimize
# ram usage, especially if the file is quite large.
	
	
